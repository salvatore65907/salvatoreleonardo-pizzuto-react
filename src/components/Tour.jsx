export default function Tour() {
    return (
        <>
            <div className="black-section" id="tour">
            <p className="white-title big-text">TOUR DATES</p>
            <p className="section-subtitle text-white">Lorem ipsum we'll play you some music.</p>
            <p className="section-subtitle text-white">Remember to book your tickets!</p>
            <div className="container my-2">    
                <div className="row">
                    <div className="col-12">
                        <a href="#">
                            <div className="reservation-month">
                                <p className="my-2 ms-2">September <span className="sold-out">Sold Out!</span></p>
                            </div>
                        </a>
                    </div>
                    <div className="col-12">
                        <a href="#">
                            <div className="reservation-month">
                                <p className="my-2 ms-2">October <span className="sold-out">Sold Out!</span></p>
                            </div>
                        </a>
                    </div>
                    <div className="col-12">
                        <a href="#">
                            <div className="reservation-month">
                                <p className="my-2 ms-2">November</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 my-1 col-md-4">
                        <div className="card">
                            <img src="/Milano-card.jpg" className="card-img-top" alt="An image of Milan" />
                            <div className="card-body">
                                <h5 className="card-title">Milan</h5>
                                <p className="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                                <a href="#" className="btn btn-dark">Buy Tickets</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 my-1 col-md-4">
                        <div className="card">
                            <img src="/NewYork-card.jpg" className="card-img-top" alt="An image of New York" />
                            <div className="card-body">
                                <h5 className="card-title">New York</h5>
                                <p className="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                                <a href="#" className="btn btn-dark">Buy Tickets</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 my-1 col-md-4">
                        <div className="card">
                            <img src="/Parigi-card.jpg" className="card-img-top" alt="An image of Paris" />
                            <div className="card-body">
                                <h5 className="card-title">Paris</h5>
                                <p className="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                                <a href="#" className="btn btn-dark">Buy Tickets</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
}