export default function Carousel() {
    return (
        <>
            <div className="container-fluid p-0" id="home">
                <div id="myCarousel" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-indicators">
                        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" className="active"
                            aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src="/Milano.jpg" className="d-block w-100 image-carousel" alt="Milan" />
                                <div className="carousel-caption d-none d-md-block carousel-caption-style">
                                    <p className="carousel-caption-title">Milan</p>
                                    <p className="carousel-caption-subtitle">Even through the traffic was a mess, we had the best in Milan.</p>
                                </div>
                        </div>
                        <div className="carousel-item">
                            <img src="/NewYork.jpg" className="d-block w-100 image-carousel" alt="New York" />
                                <div className="carousel-caption d-none d-md-block">
                                    <p className="carousel-caption-title">New York</p>
                                    <p className="carousel-caption-subtitle">Thank you New York - A night we won't forget.</p>
                                </div>
                        </div>
                        <div className="carousel-item">
                            <img src="/Parigi.jpg" className="d-block w-100 image-carousel" alt="Paris" />
                                <div className="carousel-caption d-none d-md-block">
                                    <p className="carousel-caption-title">Paris</p>
                                    <p className="carousel-caption-subtitle">The atmosphere in Paris is amazing.</p>
                                </div>
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </>
    );
}