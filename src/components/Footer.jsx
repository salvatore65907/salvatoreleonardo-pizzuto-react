export default function Footer() {
    return (
        <>
            <div className="black-section">
                <a href="#home" className="caret-link"><i className="bi bi-caret-up-fill"></i></a>
                <p className="mt-5"> <i className="bi bi-c-circle"></i> Bootstrap Theme Made By Salvatore Leonardo Pizzuto</p>
            </div>
        </>
    );
}