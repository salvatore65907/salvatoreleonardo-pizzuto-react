export default function Band() {
    return (
        <>
            <div className="white-section" id="band">
                <p className="section-title">THE BAND</p>
                <p className="section-subtitle">We love music!</p>
                <p>
                    We have created a fictional band website. lorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt,
                    ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequat
                    lorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt,
                    ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequat
                </p>
                <br />
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 my-2 col-md-4">
                            <p className="round-image-title">Milan</p>
                            <img src="/Milano-card.jpg" className="round-image" width="80%" height="80%" alt="Oval image of Milan" />
                        </div>
                        <div className="col-sm-12 my-2 col-md-4">
                            <p className="round-image-title">New York</p>
                            <img src="/NewYork-card.jpg" className="round-image" width="80%" height="80%" alt="Oval image of New York" />
                        </div>
                        <div className="col-sm-12 my-2 col-md-4">
                            <p className="round-image-title">Paris</p>
                            <img src="/Parigi-card.jpg" className="round-image" width="80%" height="80%" alt="Oval image of Paris" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}