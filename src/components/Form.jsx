export default function Form() {
    return (
        <>
            <div class="white-section" id="contact">
            <p class="section-title">Contact</p>
            <p class="section-subtitle">We love our fans!</p>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="contacts">
                            <p>Fan? Drop a note.</p>
                            <p> <i class="bi bi-geo-alt-fill"></i> Chicago, US</p>
                            <p> <i class="bi bi-phone-fill"></i> Phone: +00 1515151515</p>
                            <p> <i class="bi bi-envelope-fill"></i> Email: mail@mail.com</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <form action="..." method="POST">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 my-1">
                                        <input type="text" class="form-control resize" id="name" name="name" placeholder="Name" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 my-1">
                                        <input type="email" class="form-control resize" id="email" name="email" placeholder="Surname" />
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" rows="4" id="comment" name="comment"
                                            placeholder="Comment"></textarea>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-sm-12">
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-dark">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
}