import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Navbar from './components/Navbar'
import Carousel from './components/Carousel'
import Band from './components/Band'
import Tour from './components/Tour'
import Form from './components/Form'
import Footer from './components/Footer'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <header>
        <Navbar />
      </header>
      <main>
        <Carousel />
        <Band />
        <Tour />
        <Form />
      </main>
      <footer>
        <Footer />
      </footer>
    </>
  )
}

export default App
